/**
 * 
 * @author Matheus Marques Rodrigues
 * @version 1.0
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class App01 {

	public static void main(String[] args) {

		try {

			double av1, av2, av3, media;
			
			Scanner teclado = new Scanner(System.in);
			
			System.out.print("Digite a nota da AV1: ");
			av1 = teclado.nextDouble();
			
			while(av1 > 10 || av1 < 0) {
				System.out.print("Nota inv�lida, digite um interv�lo de 0 - 10.");
				av1 = teclado.nextDouble();
			}
			
			System.out.print("Digite a nota da AV2: ");
			av2 = teclado.nextDouble();
			
			while(av2 > 10 || av2 < 0) {
				System.out.print("Nota inv�lida, digite um interv�lo de 0 - 10.");
				av2 = teclado.nextDouble();
			}
			
			System.out.print("Digite a nota da AV3: ");
			av3 = teclado.nextDouble();
					
			while(av3 > 10 || av3 < 0) {
				System.out.print("Nota inv�lida, digite um interv�lo de 0 - 10.");
				av3 = teclado.nextDouble();
			}
			
			media = (av1+av2+av3)/3;
			
			if(media >= 7) {
				System.out.println("Voc� foi APROVADO com a m�dia: " + media);
				
			} else {
				
				System.out.println("Voc� foi REPROVADO com a m�dia: " + media);	
			}
			
		} catch(InputMismatchException e1) {
			
			System.out.println("Digite um valor n�merico.");
			
		}
		
		catch (Exception e2) {
			
			System.out.println("Deu ruim.");
			
		}
		
	}

}
