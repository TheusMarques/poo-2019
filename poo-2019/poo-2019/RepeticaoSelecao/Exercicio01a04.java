/** @author Matheus Marques Rodrigues
 *  Exerc�cio 01-04
 */

package RepeticaoSelecao;

import java.util.Scanner;

public class Exercicio01a04 {

	public static void main(String[] args) {

		int a = 0, b = 0;
		double divisao = 0;

		Scanner teclado = new Scanner(System.in);
				
		System.out.print("Digite um valor inteiro: ");
		a = teclado.nextInt();

		System.out.print("Digite outro valor inteiro: ");
		b = teclado.nextInt();

		if (b == 0) {

			System.out.println("Estrutura Do-While");

			do {

				System.out.print("Valor inv�lido! Insira um n�mero v�lido: ");
				b = teclado.nextInt();

			} while (b == 0);

			b = 0; // Criado para entrar na condi��o While - Exerc�cio 03.

			System.out.println("Estrutura While");

			while (b == 0) {

				System.out.print("Valor inv�lido! Insira um n�mero v�lido: "); //Exerc�cio 04.
				b = teclado.nextInt();

			}
		}

		divisao = a / b;

		System.out.println("A divis�o do primeiro valor pelo segundo �: " + divisao);

	}
}