package RepeticaoSelecao;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercicio05 {

	public static void main(String[] args) {

		double nota1, nota2;

		Scanner input = new Scanner(System.in);

		try {

			System.out.print("Insira a PRIMEIRA nota: ");
			nota1 = input.nextDouble();

			while (nota1 > 10 || nota1 < 0) {
				System.out.print("Nota inv�lida, insira uma nota entre 0 e 10: ");
				nota1 = input.nextDouble();
			}

			System.out.print("Insira a SEGUNDA nota: ");
			nota2 = input.nextDouble();
			
			while (nota2 > 10 || nota2 < 0) {
				System.out.print("Nota inv�lida, insira uma nota entre 0 e 10: ");
				nota2 = input.nextDouble();
			} ;

			System.out.println("A m�dia semestral foi: " + (nota1 + nota2) / 2);

		} catch (InputMismatchException error01) {

			System.out.println("Nota inv�lida.");

		}

	}
}
