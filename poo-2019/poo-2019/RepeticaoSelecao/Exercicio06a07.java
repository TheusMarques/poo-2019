package RepeticaoSelecao;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercicio06a07 {

	public static void main(String[] args) {

		double nota1, nota2;
		int escolha = 1;

		Scanner input = new Scanner(System.in);

		do {

			try {

				System.out.print("Insira a PRIMEIRA nota: ");
				nota1 = input.nextDouble();

				while (nota1 > 10 || nota1 < 0) {
					System.out
							.print("Nota inv�lida, insira uma nota entre 0 e 10: ");
					nota1 = input.nextDouble();
				}

				System.out.print("Insira a SEGUNDA nota: ");
				nota2 = input.nextDouble();

				while (nota2 > 10 || nota2 < 0) {
					System.out
							.print("Nota inv�lida, insira uma nota entre 0 e 10: ");
					nota2 = input.nextDouble();
				}
				;

				System.out.println("A m�dia semestral foi: " + (nota1 + nota2)
						/ 2);

				System.out.println("Voc� deseja executar o programa novamente? 1 [SIM] / 2 [N�O] ");
				escolha = input.nextInt();

				while (escolha > 2 || escolha < 1) {
					System.out.print("As op��es s�o apenas: 1 [SIM] / 2 [N�O]");
					escolha = input.nextInt();
				}

			} catch (InputMismatchException error01) {

				System.out.println("Nota inv�lida.");

			}

		} while (escolha == 1);

		System.out.println("Programa encerrado com sucesso!");
	}
}
